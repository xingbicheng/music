export type MusicItem = {
  name: string;
  url: string[];
};

export const musicArray: MusicItem[] = [
  {
    name: 'do',
    url: [
      require('../../../public/assets/music/20.png'),
      require('../../../public/assets/music/90.png'),
      require('../../../public/assets/music/160.png'),
      require('../../../public/assets/music/230.png'),
      require('../../../public/assets/music/300.png'),
      require('../../../public/assets/music/370.png'),
    ],
  },
  {
    name: 're',
    url: [
      require('../../../public/assets/music/30.png'),
      require('../../../public/assets/music/100.png'),
      require('../../../public/assets/music/170.png'),
      require('../../../public/assets/music/240.png'),
      require('../../../public/assets/music/310.png'),
      require('../../../public/assets/music/380.png'),
    ],
  },
  {
    name: 'mi',
    url: [
      require('../../../public/assets/music/40.png'),
      require('../../../public/assets/music/110.png'),
      require('../../../public/assets/music/180.png'),
      require('../../../public/assets/music/250.png'),
      require('../../../public/assets/music/320.png'),
      require('../../../public/assets/music/390.png'),
    ],
  },
  {
    name: 'fa',
    url: [
      require('../../../public/assets/music/50.png'),
      require('../../../public/assets/music/120.png'),
      require('../../../public/assets/music/190.png'),
      require('../../../public/assets/music/260.png'),
      require('../../../public/assets/music/330.png'),
      require('../../../public/assets/music/400.png'),
    ],
  },
  {
    name: 'so',
    url: [
      require('../../../public/assets/music/60.png'),
      require('../../../public/assets/music/130.png'),
      require('../../../public/assets/music/200.png'),
      require('../../../public/assets/music/270.png'),
      require('../../../public/assets/music/340.png'),
      require('../../../public/assets/music/410.png'),
    ],
  },
  {
    name: 'la',
    url: [
      require('../../../public/assets/music/00.png'),
      require('../../../public/assets/music/70.png'),
      require('../../../public/assets/music/140.png'),
      require('../../../public/assets/music/210.png'),
      require('../../../public/assets/music/280.png'),
      require('../../../public/assets/music/350.png'),
      require('../../../public/assets/music/420.png'),
    ],
  },
  {
    name: 'xi',
    url: [
      require('../../../public/assets/music/10.png'),
      require('../../../public/assets/music/80.png'),
      require('../../../public/assets/music/150.png'),
      require('../../../public/assets/music/220.png'),
      require('../../../public/assets/music/290.png'),
      require('../../../public/assets/music/360.png'),
    ],
  },
];
