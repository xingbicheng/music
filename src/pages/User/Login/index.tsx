import React, { useEffect, useMemo, useState } from 'react';
import {
  Button,
  Input,
  Space,
  Image,
  Radio,
  RadioChangeEvent,
  message,
  TabsProps,
  Tabs,
} from 'antd';
import { musicArray, MusicItem } from '@/pages/Music/index.data';
import TabPane from 'antd/es/tabs/TabPane';

interface CurrentItem {
  url: string;
  name: string;
}

const pickUpRandomImageData = () => {
  const randomIndex1 = Math.floor(Math.random() * musicArray.length);
  const options = musicArray[randomIndex1];
  const randomIndex2 = Math.floor(Math.random() * options.url.length);
  const url = options.url[randomIndex2];
  const name = options.name;

  return {
    url,
    name,
  };
};

const Login: React.FC = () => {
  const [queue, setQueue] = useState<CurrentItem[]>([]);
  const [answer, setAnswer] = useState<string>('');
  const [tabKey, setTabKey] = useState<string>('random');
  const [currentItem, setCurrentItem] = useState<CurrentItem>({
    url: require('../../../../public/assets/music/00.png'),
    name: '',
  });

  const onChange = (key: string) => {
    setTabKey(key);
  };

  useEffect(() => {
    const randomItem = pickUpRandomImageData();
    setCurrentItem(randomItem);
  }, []);

  const handleSelect = (e: RadioChangeEvent) => {
    if (e.target.value === 'prev') {
      handlePrev();
    } else {
      setAnswer(e.target.value);
      if (e.target.value === currentItem.name) {
        setQueue([...queue, currentItem]);
        setCurrentItem(pickUpRandomImageData());
      } else {
        message.error(`this is ${currentItem.name}`);
      }
      setAnswer('');
    }
  };

  const handlePrev = () => {
    if (queue.length === 0) {
      message.error('没有上一个');
      return;
    }
    const prevItem = queue[queue.length - 1];
    setQueue(queue.slice(1, queue.length - 1));
    setCurrentItem(prevItem);
  };

  const items: TabsProps['items'] = [
    {
      key: 'random',
      label: `Random`,
      children: (
        <div style={{ width: '500px' }}>
          <h1>Music</h1>
          <div style={{ margin: '50px 0' }}>
            <Image width={50} src={currentItem.url} />
          </div>
          <div>
            <Space.Compact style={{ width: '100%' }}>
              <Radio.Group value={answer} onChange={handleSelect}>
                <Radio.Button value="prev">Prev</Radio.Button>
                <Radio.Button value="do">1</Radio.Button>
                <Radio.Button value="re">2</Radio.Button>
                <Radio.Button value="mi">3</Radio.Button>
                <Radio.Button value="fa">4</Radio.Button>
                <Radio.Button value="so">5</Radio.Button>
                <Radio.Button value="la">6</Radio.Button>
                <Radio.Button value="xi">7</Radio.Button>
              </Radio.Group>
            </Space.Compact>
          </div>
        </div>
      ),
    },
    {
      key: '2',
      label: `All 1`,
      children: (
        <div style={{ width: '500px' }}>
          <h1>1</h1>
          <div style={{ margin: '50px 0', display: 'flex', alignItems: 'center' }}>
            {musicArray[0].url.map((item, index) => (
              <div style={{ width: '50px', marginRight: '50px' }}>
                <Image key={index} width={50} src={item} />
              </div>
            ))}
          </div>
        </div>
      ),
    },
    {
      key: '3',
      label: `All 2`,
      children: (
        <div style={{ width: '500px' }}>
          <h1>2</h1>
          <div style={{ margin: '50px 0', display: 'flex', alignItems: 'center' }}>
            {musicArray[1].url.map((item, index) => (
              <div style={{ width: '50px', marginRight: '50px' }}>
                <Image key={index} width={50} src={item} />
              </div>
            ))}
          </div>
        </div>
      ),
    },
    {
      key: '4',
      label: `All 3`,
      children: (
        <div style={{ width: '500px' }}>
          <h1>3</h1>
          <div style={{ margin: '50px 0', display: 'flex', alignItems: 'center' }}>
            {musicArray[2].url.map((item, index) => (
              <div style={{ width: '50px', marginRight: '50px' }}>
                <Image key={index} width={50} src={item} />
              </div>
            ))}
          </div>
        </div>
      ),
    },
    {
      key: '5',
      label: `All 4`,
      children: (
        <div style={{ width: '500px' }}>
          <h1>4</h1>
          <div style={{ margin: '50px 0', display: 'flex', alignItems: 'center' }}>
            {musicArray[3].url.map((item, index) => (
              <div style={{ width: '50px', marginRight: '50px' }}>
                <Image key={index} width={50} src={item} />
              </div>
            ))}
          </div>
        </div>
      ),
    },
    {
      key: '6',
      label: `All 5`,
      children: (
        <div style={{ width: '500px' }}>
          <h1>5</h1>
          <div style={{ margin: '50px 0', display: 'flex', alignItems: 'center' }}>
            {musicArray[4].url.map((item, index) => (
              <div style={{ width: '50px', marginRight: '50px' }}>
                <Image key={index} width={50} src={item} />
              </div>
            ))}
          </div>
        </div>
      ),
    },
    {
      key: '7',
      label: `All 6`,
      children: (
        <div style={{ width: '500px' }}>
          <h1>6</h1>
          <div style={{ margin: '50px 0', display: 'flex', alignItems: 'center' }}>
            {musicArray[5].url.map((item, index) => (
              <div style={{ width: '50px', marginRight: '50px' }}>
                <Image key={index} width={50} src={item} />
              </div>
            ))}
          </div>
        </div>
      ),
    },
    {
      key: '8',
      label: `All 7`,
      children: (
        <div style={{ width: '500px' }}>
          <h1>7</h1>
          <div style={{ margin: '50px 0', display: 'flex', alignItems: 'center' }}>
            {musicArray[6].url.map((item, index) => (
              <div style={{ width: '50px', marginRight: '50px' }}>
                <Image key={index} width={50} src={item} />
              </div>
            ))}
          </div>
        </div>
      ),
    },
  ];

  return (
    <div
      style={{
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      <Tabs activeKey={tabKey} onChange={onChange} items={items}></Tabs>
    </div>
  );
};

export default Login;
